package es.correos.arq.practica_arquetipo_spring_jpa.service;

import java.util.List;

import es.correos.arq.practica_arquetipo_spring_jpa.domain.Ordenes;
import es.correos.arq.practica_arquetipo_spring_jpa.dto.OrdenesDTO;
import es.correos.arq.practica_arquetipo_spring_jpa.dto.OrdenesReducidoDTO;

public interface OrdenesService {
	public List<OrdenesDTO> listarOrdenes();
	public Ordenes obtenerOrdenPorID(Integer id) throws Exception;
	public OrdenesDTO obtenerOrdenDTOPorId(Integer id) throws Exception;
	public OrdenesDTO registrarOrdenes(OrdenesDTO request) throws Exception; 
	public OrdenesDTO modificarOrdenes(OrdenesReducidoDTO request, Integer id) throws Exception;
	public void eliminarOrdenes(Integer id);
}
