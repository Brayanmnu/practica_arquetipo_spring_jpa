package es.correos.arq.practica_arquetipo_spring_jpa.service;

import es.correos.arq.practica_arquetipo_spring_jpa.dto.DetalleOrdenReducidoDTO;
import es.correos.arq.practica_arquetipo_spring_jpa.dto.DetalleOrdenesDTO;

public interface DetalleOrdenesService {
	//public Iterable<DetalleOrdenesDTO> listarDetalleOrdenesPorOrden(Long idOrden);
	public DetalleOrdenesDTO registrarDetalleOrdenes(DetalleOrdenReducidoDTO request) throws Exception;
	public DetalleOrdenesDTO modificarDetalleOrdenes(DetalleOrdenReducidoDTO request) throws Exception;
	public void elminarDetalleOrdenes(Integer idOrden,Integer idDetalle) throws Exception;
}
