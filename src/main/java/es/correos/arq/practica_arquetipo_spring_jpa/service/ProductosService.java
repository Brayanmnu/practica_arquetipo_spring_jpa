package es.correos.arq.practica_arquetipo_spring_jpa.service;

import java.util.List;

import es.correos.arq.practica_arquetipo_spring_jpa.domain.Productos;
import es.correos.arq.practica_arquetipo_spring_jpa.dto.ProductosDTO;
import es.correos.arq.practica_arquetipo_spring_jpa.dto.ProductosReducidoDTO;

public interface ProductosService {
	public List<ProductosDTO> listarProductos();
	public Productos obtenerProductosPorID(Integer id) throws Exception;
	public ProductosDTO obtenerProductosDTOPorId(Integer id) throws Exception;
	public ProductosDTO registrarProductos(ProductosDTO request) throws Exception;
	public ProductosDTO modificarProductos(ProductosReducidoDTO request, Integer id) throws Exception; 
	public void eliminarProductos(Integer id) ;
}
