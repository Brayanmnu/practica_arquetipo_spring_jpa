package es.correos.arq.practica_arquetipo_spring_jpa.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import es.correos.arq.practica_arquetipo_spring_jpa.domain.DetalleOrdenes;
import es.correos.arq.practica_arquetipo_spring_jpa.domain.DetalleOrdenesId;
import es.correos.arq.practica_arquetipo_spring_jpa.domain.Productos;
import es.correos.arq.practica_arquetipo_spring_jpa.dto.DetalleOrdenReducidoDTO;
import es.correos.arq.practica_arquetipo_spring_jpa.dto.DetalleOrdenesDTO;
import es.correos.arq.practica_arquetipo_spring_jpa.repository.DetalleOrdenesRepository;
import es.correos.arq.practica_arquetipo_spring_jpa.service.DetalleOrdenesService;
import es.correos.arq.practica_arquetipo_spring_jpa.service.OrdenesService;
import es.correos.arq.practica_arquetipo_spring_jpa.service.ProductosService;

@Service
public class DetalleOrdenesServiceImpl implements DetalleOrdenesService{

	@Autowired
	private DetalleOrdenesRepository detalleOrdenesRepository;
	
	@Autowired
	private OrdenesService ordenesService;
	@Autowired
	private ProductosService productosService;
	
	
	@Override
	public DetalleOrdenesDTO registrarDetalleOrdenes(DetalleOrdenReducidoDTO request) throws Exception {
		DetalleOrdenesId detalleOrdenesId = new DetalleOrdenesId();
		detalleOrdenesId.setOrden(ordenesService.obtenerOrdenPorID(request.getOrdenId()));
		detalleOrdenesId.setDetalleId(request.getDetalleId());
		DetalleOrdenes detalleOrdenes = new DetalleOrdenes();
		detalleOrdenes.setCantidad(request.getCantidad());
		detalleOrdenes.setDetallesId(detalleOrdenesId);
		detalleOrdenes.setProducto(productosService.obtenerProductosPorID(request.getProductoId()));
		DetalleOrdenes detalleOrdenesOut =detalleOrdenesRepository.save(detalleOrdenes);
		DetalleOrdenesDTO detalleOrdenesDTO = new DetalleOrdenesDTO();
		detalleOrdenesDTO.setCantidad(detalleOrdenesOut.getCantidad());
		detalleOrdenesDTO.setDetalleId(detalleOrdenesOut.getDetallesId().getDetalleId());
		detalleOrdenesDTO.setOrden(ordenesService.obtenerOrdenDTOPorId(detalleOrdenes
				.getDetallesId().getOrden().getOrdenId()));
		detalleOrdenesDTO.setProducto(productosService
				.obtenerProductosDTOPorId(detalleOrdenes.getProducto().getProductoId()));
	
		return detalleOrdenesDTO;
	}

	@Override
	public DetalleOrdenesDTO modificarDetalleOrdenes(DetalleOrdenReducidoDTO request) throws Exception {
		DetalleOrdenesId detalleOrdenesId = new DetalleOrdenesId();
		detalleOrdenesId.setDetalleId(request.getDetalleId());
		detalleOrdenesId.setOrden(ordenesService.obtenerOrdenPorID(request.getOrdenId()));
		Productos productos = productosService.obtenerProductosPorID(request.getProductoId());
		DetalleOrdenes detalleOrdenes =  detalleOrdenesRepository
				.findByDetallesId(detalleOrdenesId)
				.map(detalleOrdenesUpdate->{
					detalleOrdenesUpdate.setCantidad(request.getCantidad());
					detalleOrdenesUpdate.setProducto(productos);
					detalleOrdenesRepository.save(detalleOrdenesUpdate);
					return detalleOrdenesUpdate;
				}).orElseThrow(()-> new ResponseStatusException(HttpStatus.NOT_FOUND,"Orden no encontrada"));
		DetalleOrdenesDTO detalleOrdenesDTO = new DetalleOrdenesDTO();
		detalleOrdenesDTO.setCantidad(detalleOrdenes.getCantidad());
		detalleOrdenesDTO.setDetalleId(detalleOrdenes.getDetallesId().getDetalleId());
		detalleOrdenesDTO.setOrden(ordenesService.obtenerOrdenDTOPorId(detalleOrdenes
				.getDetallesId().getOrden().getOrdenId()));
		detalleOrdenesDTO.setProducto(productosService.obtenerProductosDTOPorId(detalleOrdenes
				.getProducto().getProductoId()));
		return detalleOrdenesDTO;
		
	}
	
	

	@Override
	public void elminarDetalleOrdenes(Integer idOrden,Integer idDetalle) throws Exception {
		// TODO Auto-generated method stub
		DetalleOrdenesId detalleOrdenesId = new DetalleOrdenesId();
		detalleOrdenesId.setDetalleId(idDetalle);
		detalleOrdenesId.setOrden(ordenesService.obtenerOrdenPorID(idOrden));
		detalleOrdenesRepository.deleteById(detalleOrdenesId);
		
	}

}
