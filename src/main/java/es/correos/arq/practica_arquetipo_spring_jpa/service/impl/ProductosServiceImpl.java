package es.correos.arq.practica_arquetipo_spring_jpa.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import es.correos.arq.practica_arquetipo_spring_jpa.domain.Productos;
import es.correos.arq.practica_arquetipo_spring_jpa.dto.ProductosDTO;
import es.correos.arq.practica_arquetipo_spring_jpa.dto.ProductosReducidoDTO;
import es.correos.arq.practica_arquetipo_spring_jpa.repository.ProductosRepository;
import es.correos.arq.practica_arquetipo_spring_jpa.service.ProductosService;

@Service
public class ProductosServiceImpl implements ProductosService{

	@Autowired
	private ProductosRepository productosRepository;

	
	@Override
	public List<ProductosDTO> listarProductos() {
		List<Productos> listaProductos = productosRepository.findAll();
		List<ProductosDTO> listaProdDTO = new ArrayList<>();
		for (Productos prod: listaProductos) {
			ProductosDTO productosDTO = new ProductosDTO();
			productosDTO.setCategoriaId(prod.getCategoriaId());
			productosDTO.setDescripcion(prod.getDescripcion());
			productosDTO.setExistencia(prod.getExistencia());
			productosDTO.setPrecioUnit(prod.getPrecioUnit());
			productosDTO.setProductoId(prod.getProductoId());
			productosDTO.setProveedorId(prod.getProveedorId());
			listaProdDTO.add(productosDTO);
		}
		return listaProdDTO;
	}


	@Override
	public Productos obtenerProductosPorID(Integer id) throws Exception {
		return productosRepository.findById(id).orElseThrow(()->new Exception("Producto no encontrado"));
	}

	@Override
	public ProductosDTO registrarProductos(ProductosDTO request) throws Exception {
		Productos productosSave = new Productos();
		productosSave.setCategoriaId(request.getCategoriaId());
		productosSave.setDescripcion(request.getDescripcion());
		productosSave.setExistencia(request.getExistencia());
		productosSave.setPrecioUnit(request.getPrecioUnit());
		productosSave.setProveedorId(request.getProveedorId());
		productosSave.setProductoId(request.getProductoId());
		Productos productosOut = productosRepository.save(productosSave);
		ProductosDTO productosDTO = new ProductosDTO();
		productosDTO.setCategoriaId(productosOut.getCategoriaId());
		productosDTO.setDescripcion(productosOut.getDescripcion());
		productosDTO.setExistencia(productosOut.getExistencia());
		productosDTO.setPrecioUnit(productosOut.getPrecioUnit());
		productosDTO.setProductoId(productosOut.getProductoId());
		productosDTO.setProveedorId(productosOut.getProveedorId());
		return productosDTO;
	}

	@Override
	public ProductosDTO modificarProductos(ProductosReducidoDTO request, Integer id) throws Exception {
		Productos productos = new Productos();
		productos.setCategoriaId(request.getCategoriaId());
		productos.setDescripcion(request.getDescripcion());
		productos.setExistencia(request.getExistencia());
		productos.setPrecioUnit(request.getPrecioUnit());
		productos.setProductoId(id);
		productos.setProveedorId(request.getProveedorId());
		Productos productosOut = productosRepository.findById(id)
		        .map(productoUpdate -> {
		        	productoUpdate.setCategoriaId(productos.getCategoriaId());
		        	productoUpdate.setDescripcion(productos.getDescripcion());
		        	productoUpdate.setExistencia(productos.getExistencia());
		        	productoUpdate.setPrecioUnit(productos.getPrecioUnit());
		        	productoUpdate.setProveedorId(productos.getProveedorId());
		        	productosRepository.save(productoUpdate);
		            return productoUpdate;
		    }).orElseThrow(()-> new ResponseStatusException(HttpStatus.NOT_FOUND,"Producto no encontrado"));
		ProductosDTO productosDTO = new ProductosDTO();
		productosDTO.setCategoriaId(productosOut.getCategoriaId());
		productosDTO.setDescripcion(productosOut.getDescripcion());
		productosDTO.setExistencia(productosOut.getExistencia());
		productosDTO.setPrecioUnit(productosOut.getPrecioUnit());
		productosDTO.setProductoId(productosOut.getProductoId());
		productosDTO.setProveedorId(productosOut.getProveedorId());
		return productosDTO;
	}

	@Override
	public void eliminarProductos(Integer id){
		productosRepository.deleteById(id);
	}


	@Override
	public ProductosDTO obtenerProductosDTOPorId(Integer id) throws Exception {
		ProductosDTO productosDTO = new ProductosDTO();
		Productos productos = productosRepository.findById(id).orElseThrow(()->new Exception("Producto no encontrado"));
		productosDTO.setCategoriaId(productos.getCategoriaId());
		productosDTO.setDescripcion(productos.getDescripcion());
		productosDTO.setExistencia(productos.getExistencia());
		productosDTO.setPrecioUnit(productos.getPrecioUnit());
		productosDTO.setProductoId(productos.getProductoId());
		productosDTO.setProveedorId(productos.getProveedorId());
		return productosDTO;
	}
	
}
