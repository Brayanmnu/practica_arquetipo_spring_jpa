package es.correos.arq.practica_arquetipo_spring_jpa.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import es.correos.arq.practica_arquetipo_spring_jpa.domain.Ordenes;
import es.correos.arq.practica_arquetipo_spring_jpa.dto.OrdenesDTO;
import es.correos.arq.practica_arquetipo_spring_jpa.dto.OrdenesReducidoDTO;
import es.correos.arq.practica_arquetipo_spring_jpa.repository.OrdenesRepository;
import es.correos.arq.practica_arquetipo_spring_jpa.service.OrdenesService;


@Service
public class OrdenesServiceImpl implements OrdenesService{
	@Autowired
	private OrdenesRepository ordenesRepository;
	
	@Override
	public List<OrdenesDTO> listarOrdenes() {
		List<Ordenes> listaOrdenes = ordenesRepository.findAll();
		List<OrdenesDTO> listaOrdenesDTO = new ArrayList<>();
		for(Ordenes orden: listaOrdenes) {
			OrdenesDTO ordenesDTO = new OrdenesDTO();
			ordenesDTO.setClienteId(orden.getClienteId());
			ordenesDTO.setDescuento(orden.getDescuento());
			ordenesDTO.setEmpleadoId(orden.getEmpleadoId());
			ordenesDTO.setFechaOrden(orden.getFechaOrden());
			ordenesDTO.setOrdenId(orden.getOrdenId());
			listaOrdenesDTO.add(ordenesDTO);
		}
		return listaOrdenesDTO;
	}

	@Override
	public OrdenesDTO registrarOrdenes(OrdenesDTO request) throws Exception{
		Ordenes ordenesSave = new Ordenes();
		ordenesSave.setClienteId(request.getClienteId());
		ordenesSave.setDescuento(request.getDescuento());
		ordenesSave.setEmpleadoId(request.getEmpleadoId());
		ordenesSave.setFechaOrden(request.getFechaOrden());
		ordenesSave.setOrdenId(request.getOrdenId());
		Ordenes ordenOut = ordenesRepository.save(ordenesSave);
		OrdenesDTO ordenesDTO = new OrdenesDTO();
		ordenesDTO.setClienteId(ordenOut.getClienteId());
		ordenesDTO.setDescuento(ordenOut.getDescuento());
		ordenesDTO.setEmpleadoId(ordenOut.getEmpleadoId());
		ordenesDTO.setFechaOrden(ordenOut.getFechaOrden());
		ordenesDTO.setOrdenId(ordenOut.getOrdenId());
		return ordenesDTO;
	}

	@Override
	public OrdenesDTO modificarOrdenes(OrdenesReducidoDTO request, Integer id) throws Exception{
		Ordenes ordenes = new Ordenes();
		ordenes.setClienteId(request.getClienteId());
		ordenes.setDescuento(request.getDescuento());
		ordenes.setEmpleadoId(request.getEmpleadoId());
		ordenes.setFechaOrden(request.getFechaOrden());
		ordenes.setOrdenId(id);
		Ordenes ordenesOut = ordenesRepository.findById(id)
				.map(ordenesUpdate->{
					ordenesUpdate.setClienteId(ordenes.getClienteId());
					ordenesUpdate.setDescuento(ordenes.getDescuento());
					ordenesUpdate.setEmpleadoId(ordenes.getEmpleadoId());
					ordenesUpdate.setFechaOrden(ordenes.getFechaOrden());
					ordenesRepository.save(ordenes);
					return ordenesUpdate;
				}) .orElseThrow(()-> new ResponseStatusException(HttpStatus.NOT_FOUND,"Orden no encontrada"));
		OrdenesDTO ordenesDTO = new OrdenesDTO();
		ordenesDTO.setClienteId(ordenesOut.getClienteId());
		ordenesDTO.setDescuento(ordenes.getDescuento());
		ordenesDTO.setEmpleadoId(ordenesOut.getEmpleadoId());
		ordenesDTO.setFechaOrden(ordenes.getFechaOrden());
		ordenesDTO.setOrdenId(ordenes.getOrdenId());
		return ordenesDTO;
	}

	@Override
	public Ordenes obtenerOrdenPorID(Integer id) throws Exception {
		return ordenesRepository.findById(id).orElseThrow(()->new Exception("Orden no encontrada"));
	}

	@Override
	public void eliminarOrdenes(Integer id) {
		ordenesRepository.deleteById(id);
	}

	@Override
	public OrdenesDTO obtenerOrdenDTOPorId(Integer id) throws Exception {
		OrdenesDTO ordenesDTO = new OrdenesDTO();
		Ordenes ordenes = ordenesRepository.findById(id).orElseThrow(()->new Exception("Orden no encontrada"));
		ordenesDTO.setClienteId(ordenes.getClienteId());
		ordenesDTO.setDescuento(ordenes.getDescuento());
		ordenesDTO.setEmpleadoId(ordenes.getEmpleadoId());
		ordenesDTO.setFechaOrden(ordenes.getFechaOrden());
		ordenesDTO.setOrdenId(ordenes.getOrdenId());
		return ordenesDTO;
	}


}
