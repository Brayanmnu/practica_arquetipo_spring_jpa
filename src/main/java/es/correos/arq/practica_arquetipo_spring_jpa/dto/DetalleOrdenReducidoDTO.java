package es.correos.arq.practica_arquetipo_spring_jpa.dto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DetalleOrdenReducidoDTO {
	private Integer ordenId;
	private Integer detalleId;
	private Integer productoId;
	private Integer cantidad;
}
