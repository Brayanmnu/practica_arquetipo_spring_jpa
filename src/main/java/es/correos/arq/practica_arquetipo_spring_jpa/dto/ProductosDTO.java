package es.correos.arq.practica_arquetipo_spring_jpa.dto;

import java.math.BigDecimal;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProductosDTO {
	private Integer productoId;
	private Integer proveedorId;
	private Integer categoriaId;
	private Character descripcion;
	private BigDecimal precioUnit;
	private Integer existencia;

}
