package es.correos.arq.practica_arquetipo_spring_jpa.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DetalleOrdenesDTO {
	private OrdenesDTO orden;
	private Integer detalleId;
	private ProductosDTO producto;
	private Integer cantidad;
}
