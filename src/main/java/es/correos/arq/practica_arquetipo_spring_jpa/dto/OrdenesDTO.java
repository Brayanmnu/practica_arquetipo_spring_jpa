package es.correos.arq.practica_arquetipo_spring_jpa.dto;


import java.time.LocalDate;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OrdenesDTO{
	private Integer ordenId;
	private Integer empleadoId;
	private Integer clienteId;
	private LocalDate fechaOrden;
	private Integer descuento;
}
