package es.correos.arq.practica_arquetipo_spring_jpa.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import es.correos.arq.practica_arquetipo_spring_jpa.dto.DetalleOrdenReducidoDTO;
import es.correos.arq.practica_arquetipo_spring_jpa.dto.DetalleOrdenesDTO;
import es.correos.arq.practica_arquetipo_spring_jpa.dto.OrdenesDTO;
import es.correos.arq.practica_arquetipo_spring_jpa.dto.OrdenesReducidoDTO;
import es.correos.arq.practica_arquetipo_spring_jpa.dto.ProductosDTO;
import es.correos.arq.practica_arquetipo_spring_jpa.dto.ProductosReducidoDTO;
import es.correos.arq.practica_arquetipo_spring_jpa.service.DetalleOrdenesService;
import es.correos.arq.practica_arquetipo_spring_jpa.service.OrdenesService;
import es.correos.arq.practica_arquetipo_spring_jpa.service.ProductosService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api")
@Api(value = "/api", tags = "Operaciones con usuarios")
public class ApiController {
	
	@Autowired
	ProductosService productosService;


	@Autowired
	OrdenesService ordenesService;
	
	@Autowired
	DetalleOrdenesService detalleOrdenesService;
	
	@ApiOperation(value = "Acceso a  todos los usuario.",
			notes = "Se pide el listado de todas los ordenes dados de alta en la BBDD.")
	@GetMapping(value="/ordenesListar", headers = "Accept=application/json")
	@ApiResponse(code = 401, message = "No se encontro el resultado")

	public ResponseEntity<List<OrdenesDTO>> listarOrdenes(){
		return  new ResponseEntity<>(ordenesService.listarOrdenes(),HttpStatus.OK);
	}
	
	@PostMapping("/ordenRegistrar")
	public ResponseEntity<OrdenesDTO> registrarOrden(@RequestBody OrdenesDTO request) throws Exception {
		return  new ResponseEntity<>(ordenesService
				.registrarOrdenes(request), HttpStatus.CREATED);
	}
	
	@PutMapping("ordenModficar/{id}")
	public ResponseEntity<OrdenesDTO> modificarOrden(@PathVariable("id") Integer id
			,@RequestBody OrdenesReducidoDTO request) throws Exception{
		return new ResponseEntity<>(ordenesService
				.modificarOrdenes(request,id), HttpStatus.OK);
	}
	
	@DeleteMapping("ordenEliminar/{id}")
	public void eliminarOrden(@PathVariable("id") Integer id) throws Exception{ 
		ordenesService.eliminarOrdenes(id);
	}
	
	
	@ApiOperation(value = "Acceso a  todos los usuario.",
			notes = "Se pide el listado de todas los productos dados de alta en la BBDD.")
	@GetMapping(value="/productosListar", headers = "Accept=application/json")
	@ApiResponse(code = 401, message = "No se encontro el resultado")
	public List<ProductosDTO> listarProductos(){
		return productosService.listarProductos();
	}
	

	@PostMapping("/productoRegistrar")
	public  ResponseEntity<ProductosDTO> registrarProductos (@RequestBody ProductosDTO request) 
			throws Exception {
		return new ResponseEntity<>(productosService
				.registrarProductos(request), HttpStatus.CREATED);
	}
	
	@PutMapping("productoModificar/{id}")
	public ResponseEntity<ProductosDTO> modificarProductos(@PathVariable("id") Integer id
			, @RequestBody ProductosReducidoDTO request) throws Exception{ 
		return new ResponseEntity<>(productosService
				.modificarProductos(request,id), HttpStatus.OK);
	}
	
	@DeleteMapping("productoEliminar/{id}")
	public void eliminarProducto(@PathVariable("id") Integer id)throws Exception{ 
		productosService.eliminarProductos(id);
	}
	
	
	@PostMapping("/detalleOrdenRegistrar")
	public ResponseEntity<DetalleOrdenesDTO> registrarDetalleOrden(@RequestBody 
			DetalleOrdenReducidoDTO request) throws Exception{
		return new ResponseEntity<>(detalleOrdenesService.registrarDetalleOrdenes(request), HttpStatus.OK);
	}
	
	@PutMapping("detalleOrdenModificar")
	public ResponseEntity<DetalleOrdenesDTO> modificarProductos(@RequestBody DetalleOrdenReducidoDTO request) throws Exception{ 
		return new ResponseEntity<>(detalleOrdenesService.modificarDetalleOrdenes(request), HttpStatus.OK);
	}
	
	
	@DeleteMapping("detalleOrdenEliminar/{idOrden}/{idDetalle}")
	public void eliminarDetalleOrden(@PathVariable("idOrden") Integer idOrden
			,@PathVariable("idDetalle") Integer idDetalle)throws Exception{ 
		detalleOrdenesService.elminarDetalleOrdenes(idOrden, idDetalle);
	}
}
	
	
	
	
	
	
	
	