package es.correos.arq.practica_arquetipo_spring_jpa.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import es.correos.arq.practica_arquetipo_spring_jpa.domain.Productos;


@Repository
public interface ProductosRepository extends JpaRepository<Productos, Integer>{
	public Optional<Productos> findByProductoId(Integer productoId);

}
