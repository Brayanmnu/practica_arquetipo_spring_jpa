package es.correos.arq.practica_arquetipo_spring_jpa.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import es.correos.arq.practica_arquetipo_spring_jpa.domain.Ordenes;

@Repository
public interface OrdenesRepository extends JpaRepository<Ordenes,Integer>{

	public Optional<Ordenes> findByOrdenId(Integer ordenId);
}
