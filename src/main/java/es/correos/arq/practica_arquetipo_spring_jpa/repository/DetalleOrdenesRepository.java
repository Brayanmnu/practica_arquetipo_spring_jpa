package es.correos.arq.practica_arquetipo_spring_jpa.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import es.correos.arq.practica_arquetipo_spring_jpa.domain.DetalleOrdenes;
import es.correos.arq.practica_arquetipo_spring_jpa.domain.DetalleOrdenesId;

@Repository
public interface DetalleOrdenesRepository extends JpaRepository<DetalleOrdenes, DetalleOrdenesId>{
	
	public Optional<DetalleOrdenes> findByDetallesId(DetalleOrdenesId detallesId); 
}
