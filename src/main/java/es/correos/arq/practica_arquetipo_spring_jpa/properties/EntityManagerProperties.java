/**
 * 
 */
package es.correos.arq.practica_arquetipo_spring_jpa.properties;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author jbarriov
 *
 */
@NoArgsConstructor
@Getter
@Setter
public class EntityManagerProperties {
	private String[] packagesToScan;
}	
