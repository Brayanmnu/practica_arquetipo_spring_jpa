package es.correos.arq.practica_arquetipo_spring_jpa.domain;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="DETALLE_ORDENES")
public class DetalleOrdenes {
	@EmbeddedId
	private DetalleOrdenesId detallesId;
	@ManyToOne
	@JoinColumn(name="PRODUCTOID")
	private Productos producto;
	@Column(name="CANTIDAD")
	private Integer cantidad;
}
