package es.correos.arq.practica_arquetipo_spring_jpa.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;



@Data
@AllArgsConstructor
@NoArgsConstructor
@Embeddable
public class DetalleOrdenesId implements Serializable{
	@ManyToOne
	@JoinColumn(name="ORDENID")
	private Ordenes orden;
	@Column(name="DETALLEID")
	private Integer detalleId;
}
