package es.correos.arq.practica_arquetipo_spring_jpa.domain;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="PRODUCTOS")
public class Productos {
	@Id
	@Column(name="PRODUCTOID")
	private Integer productoId;
	@Column(name="PROVEEDORID")
	private Integer proveedorId;
	@Column(name="CATEGORIAID")
	private Integer categoriaId;
	@Column(name="DESCRIPCION")
	private Character descripcion;
	@Column(name="PRECIOUNIT")
	private BigDecimal precioUnit;
	@Column(name="EXISTENCIA")
	private Integer existencia;
}
