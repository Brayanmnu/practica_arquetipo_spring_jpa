package es.correos.arq.practica_arquetipo_spring_jpa.domain;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="ORDENES")
public class Ordenes {
	@Id
	@Column(name="ORDENID")
	private Integer ordenId;
	@Column(name="EMPLEADOID")
	private Integer empleadoId;
	@Column(name="CLIENTEID")
	private Integer clienteId;
	@Column(name="FECHAORDEN")
	private LocalDate fechaOrden;
	@Column(name="DESCUENTO")
	private Integer descuento;
}
